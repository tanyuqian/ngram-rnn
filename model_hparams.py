num_units = 256
beam_width = 10

encoder_hparams = {
    'rnn_cell_fw': {
        'kwargs': {
            'num_units': num_units
        }
    }
}

cell_hparams = {
    'kwargs': {
        'num_units': num_units
    }
}

embedder_hparams = {'dim': num_units}

decoder_hparams = {
    'attention': {
        'kwargs': {
            'num_units': num_units
        },
        'attention_layer_size': num_units
    }
}
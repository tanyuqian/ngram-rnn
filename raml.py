"""Attentional Seq2seq.
"""
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import importlib
import tensorflow as tf
import texar as tx
import numpy as np
import random

from ngram_attn_decoder import NGramAttnRNNDecoder
from loss_fn import ngram_loss_fn_with_rewards

flags = tf.flags

flags.DEFINE_string("config_model", "config_model", "The model config.")
flags.DEFINE_string("config_data", "config_iwslt14", "The dataset config.")

flags.DEFINE_integer('k', 1, '')
flags.DEFINE_float('c', 0.0, '')
flags.DEFINE_float('d', 0.0, '')
flags.DEFINE_float('e', 0.0, '')

flags.DEFINE_string('raml_file', '', '')
flags.DEFINE_integer('n_samples', None, '')
flags.DEFINE_float('tau', 0.85, '')

flags.DEFINE_integer('set_no', 0, '')

FLAGS = flags.FLAGS

config_model = importlib.import_module(FLAGS.config_model)
config_data = importlib.import_module(FLAGS.config_data)

set_dir = 'result_set' + str(FLAGS.set_no) + '/'
log_dir = set_dir + FLAGS.config_data.split('_')[-1] +\
          '_training_log' + '_raml' + str(FLAGS.n_samples) +\
          '_k' + str(FLAGS.k) + '_c' + str(FLAGS.c) +\
          '_d' + str(FLAGS.d) + '_e' + str(FLAGS.e) + '/'
os.system('mkdir ' + set_dir)
os.system('mkdir ' + log_dir)


def read_raml_sample_file():
    raml_file = open(FLAGS.raml_file)

    train_data = []
    sample_num = -1
    for line in raml_file.readlines():
        line = line.rstrip('\n')
        if line.startswith('***'):
            continue
        elif line.endswith('samples'):
            sample_num = eval(line.split()[0])
            assert sample_num == 1 or sample_num == FLAGS.n_samples
        elif line.startswith('source:'):
            train_data.append({'source': line[7:], 'targets': []})
        else:
            train_data[-1]['targets'].append(line.split('|||'))
            if sample_num == 1:
                for i in range(FLAGS.n_samples - 1):
                    train_data[-1]['targets'].append(line.split('|||'))
    return train_data


def build_model(batch, train_data, rewards):
    """Assembles the seq2seq model.
    """
    source_embedder = tx.modules.WordEmbedder(
        vocab_size=train_data.source_vocab.size, hparams=config_model.embedder)

    encoder = tx.modules.BidirectionalRNNEncoder(
        hparams=config_model.encoder)

    enc_outputs, _ = encoder(source_embedder(batch['source_text_ids']))

    target_embedder = tx.modules.WordEmbedder(
        vocab_size=train_data.target_vocab.size, hparams=config_model.embedder)

    decoder = NGramAttnRNNDecoder(
        memory=tf.concat(enc_outputs, axis=2),
        memory_sequence_length=batch['source_length'],
        vocab_size=train_data.target_vocab.size,
        embedding=target_embedder,
        next_k=FLAGS.k,
        hparams=config_model.decoder)

    training_outputs, _, _ = decoder(
        decoding_strategy='train_greedy',
        inputs=target_embedder(batch['target_text_ids'][:, :-1]),
        sequence_length=batch['target_length'] - 1)

    train_op = tx.core.get_train_op(
        ngram_loss_fn_with_rewards(batch, training_outputs, rewards, FLAGS))

    start_tokens = tf.ones_like(
        batch['target_length']) * train_data.target_vocab.bos_token_id
    beam_search_outputs, _, _ = \
        tx.modules.beam_search_decode(
            decoder_or_cell=decoder,
            embedding=target_embedder,
            start_tokens=start_tokens,
            end_token=train_data.target_vocab.eos_token_id,
            beam_width=config_model.beam_width,
            max_decoding_length=60)

    return train_op, beam_search_outputs


def main():
    """Entrypoint.
    """
    config_data.train['batch_size'] *= FLAGS.n_samples
    config_data.val['batch_size'] *= FLAGS.n_samples
    config_data.test['batch_size'] *= FLAGS.n_samples

    train_data = tx.data.PairedTextData(hparams=config_data.train)
    val_data = tx.data.PairedTextData(hparams=config_data.val)
    test_data = tx.data.PairedTextData(hparams=config_data.test)
    data_iterator = tx.data.TrainTestDataIterator(
        train=train_data, val=val_data, test=test_data)

    batch = data_iterator.get_next()
    rewards_ts = tf.placeholder(
        dtype=tf.float32, shape=[None, ], name='training_rewards')

    train_op, infer_outputs = build_model(batch, train_data, rewards_ts)

    raml_train_data = read_raml_sample_file()

    def _train_epoch(sess, epoch_no):
        data_iterator.switch_to_train_data(sess)
        training_log_file = \
            open(log_dir + 'training_log' + str(epoch_no) + '.txt', 'w')

        step = 0
        source_buffer, target_buffer = [], []
        random.shuffle(raml_train_data)
        for training_pair in raml_train_data:
            for target in training_pair['targets']:
                source_buffer.append(training_pair['source'])
                target_buffer.append(target)

            if len(target_buffer) != train_data.batch_size:
                continue

            source_ids = []
            source_length = []
            target_ids = []
            target_length = []
            scores = []

            for sentence in source_buffer:
                ids = [train_data.source_vocab.token_to_id_map_py[token] for
                       token in sentence.split()]
                ids = ids + [train_data.source_vocab.eos_token_id]

                source_ids.append(ids)
                source_length.append(len(ids))

            for sentence, score_str in target_buffer:
                ids = [train_data.target_vocab.bos_token_id]
                ids = ids + [train_data.target_vocab.token_to_id_map_py[
                                 token] for token in sentence.split()]
                ids = ids + [train_data.target_vocab.eos_token_id]

                target_ids.append(ids)
                scores.append(eval(score_str))
                target_length.append(len(ids))

            rewards = []
            for i in range(0, train_data.batch_size, FLAGS.n_samples):
                tmp = np.array(scores[i:i + FLAGS.n_samples])
                rewards.append(
                    np.exp(tmp / FLAGS.tau) / np.sum(np.exp(tmp / FLAGS.tau)))

            reward_norm = []
            for i in range(train_data.batch_size // FLAGS.n_samples):
                for j in range(0, FLAGS.n_samples):
                    reward_norm.append(rewards[i][j])

            for value in source_ids:
                while len(value) < max(source_length):
                    value.append(0)
            for value in target_ids:
                while len(value) < max(target_length):
                    value.append(0)

            feed_dict = {
                tx.global_mode(): tf.estimator.ModeKeys.TRAIN,
                batch['source_text_ids']: np.array(source_ids),
                batch['target_text_ids']: np.array(target_ids),
                batch['source_length']: np.array(source_length),
                batch['target_length']: np.array(target_length),
                rewards_ts: np.array(reward_norm)
            }
            source_buffer = []
            target_buffer = []

            loss = sess.run(train_op, feed_dict=feed_dict)
            print("step={}, loss={:.4f}".format(step, loss),
                  file=training_log_file)
            training_log_file.flush()
            step += 1

    def _eval_epoch(sess, mode):
        if mode == 'val':
            data_iterator.switch_to_val_data(sess)
        else:
            data_iterator.switch_to_test_data(sess)

        refs, hypos = [], []
        while True:
            try:
                fetches = [
                    batch['target_text'][:, 1:],
                    infer_outputs.predicted_ids[:, :, 0]
                ]
                feed_dict = {
                    tx.global_mode(): tf.estimator.ModeKeys.EVAL
                }
                target_texts_ori, output_ids = \
                    sess.run(fetches, feed_dict=feed_dict)

                target_texts = tx.utils.strip_special_tokens(target_texts_ori)
                output_texts = tx.utils.map_ids_to_strs(
                    ids=output_ids, vocab=val_data.target_vocab)

                for hypo, ref in zip(output_texts, target_texts):
                    hypos.append(hypo)
                    refs.append([ref])
            except tf.errors.OutOfRangeError:
                break

        return tx.evals.corpus_bleu_moses(
            list_of_references=refs, hypotheses=hypos)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        sess.run(tf.tables_initializer())

        best_val_bleu = -1.
        scores_file = open(log_dir + 'scores.txt', 'w')
        for i in range(config_data.num_epochs):
            _train_epoch(sess, i)

            val_bleu = _eval_epoch(sess, 'val')
            best_val_bleu = max(best_val_bleu, val_bleu)
            print('val epoch={}, BLEU={:.4f}; best-ever={:.4f}'.format(
                i, val_bleu, best_val_bleu), file=scores_file)

            test_bleu = _eval_epoch(sess, 'test')
            print('test epoch={}, BLEU={:.4f}'.format(i, test_bleu),
                  file=scores_file)

            print('=' * 50, file=scores_file)
            scores_file.flush()


if __name__ == '__main__':
    main()

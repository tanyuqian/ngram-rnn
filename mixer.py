from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import tensorflow as tf
from tensorflow.contrib import seq2seq

import texar as tx
from ngram_decoder import NGramRNNDecoder

import sys
import os
import argparse

from nltk.translate import bleu_score
from rouge import Rouge

import mt_configs, ts_configs

from mixer_helpers import MixerGreedyHelper, MixerSamplingHelper
import numpy as np

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--k', type=int, default=1, choices=[1])
arg_parser.add_argument('--c', type=float, default=0., choices=[0.])
arg_parser.add_argument('--d', type=float, default=0., choices=[0.])
arg_parser.add_argument('--e', type=float, default=0., choices=[0.])
arg_parser.add_argument('--xent', type=int)
arg_parser.add_argument('--xer', type=int)
arg_parser.add_argument('--delta', type=int)
arg_parser.add_argument('--length_t', type=int)
arg_parser.add_argument('--num_units', type=int, default=512)
arg_parser.add_argument('--num_epochs', type=int, default=15)
arg_parser.add_argument('--dropout', type=float, default=0.2)
arg_parser.add_argument('--beam_width', type=int, default=10)
arg_parser.add_argument('--task', type=str, choices=['mt', 'ts'])

args = arg_parser.parse_args()

log_dir = args.task + '_training_log' + '_mixer' + \
          '_xent' + str(args.xent) + '_xer' + str(args.xer) + \
          '_delta' + str(args.delta) + '_lengtht' + str(args.length_t) + '/'
os.system('mkdir ' + log_dir)
valid_test_log_file = open(log_dir + 'valid_test_log.txt', 'w')


def encode(data_batch, vocab_size):
    embedder = tx.modules.WordEmbedder(
        vocab_size=vocab_size, hparams={'dim': args.num_units})

    encoder_hparams = tx.modules.BidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell_fw']['kwargs']['num_units'] = args.num_units
    encoder_hparams['rnn_cell_fw']['dropout']['input_keep_prob'] = \
        1. - args.dropout
    encoder = tx.modules.BidirectionalRNNEncoder(hparams=encoder_hparams)

    enc_outputs, enc_last = \
        encoder(inputs=embedder(data_batch['source_text_ids']))

    return enc_outputs, enc_last


def loss_fn(data_batch, output, advantages, rl_point, length):
    mle_loss = tx.losses.sequence_sparse_softmax_cross_entropy(
        labels=data_batch['target_text_ids'][:, 1:rl_point + 1],
        logits=output.logits[:, :rl_point],
        sequence_length=tf.minimum(rl_point, data_batch['target_length'] - 1))

    rl_loss = tx.losses.sequence_sparse_softmax_cross_entropy(
        labels=output.sample_id[:, rl_point:],
        logits=output.logits[:, rl_point:],
        sequence_length=tf.maximum(0, length - rl_point),
        average_across_batch=False)

    return mle_loss + tf.reduce_mean(rl_loss * advantages)


def get_train_op(data_batch, decoder_cell, embedder, enc_outputs, enc_last,
                 batch_size, target_vocab_size, advantages,
                 rl_point, target_bos_token_id, target_eos_token_id):
    decoder_hparams = NGramRNNDecoder.default_hparams()
    decoder_hparams['next_k'] = args.k
    decoder = NGramRNNDecoder(
        cell=decoder_cell,
        vocab_size=target_vocab_size,
        embedding=embedder,
        hparams=decoder_hparams)

    sampling_helper = MixerSamplingHelper(
        embedding=embedder,
        inputs=data_batch['target_text_ids'],
        sequence_length=data_batch['target_length'] - 1,
        rl_point=rl_point,
        start_tokens=[target_bos_token_id] * batch_size,
        end_token=target_eos_token_id)

    greedy_helper = MixerGreedyHelper(
        embedding=embedder,
        inputs=data_batch['target_text_ids'],
        sequence_length=data_batch['target_length'] - 1,
        rl_point=rl_point,
        start_tokens=[target_bos_token_id] * batch_size,
        end_token=target_eos_token_id)

    sampling_output, _, sampling_length = decoder(
        helper=sampling_helper, initial_state=decoder.zero_state(
            batch_size=batch_size, dtype=tf.float32))

    greedy_output, _, _ = decoder(
        helper=greedy_helper, initial_state=decoder.zero_state(
            batch_size=batch_size, dtype=tf.float32))

    return sampling_output, greedy_output, decoder, tx.core.get_train_op(
        loss_fn(data_batch, sampling_output, advantages,
                rl_point, sampling_length),
        hparams={'optimizer': {'type': 'AdamOptimizer'}})


def get_infer_outputs(data_batch, decoder_cell, embedder,
                      enc_outputs, enc_last, batch_size, target_vocab_size,
                      target_bos_token_id, target_eos_token_id,
                      training_decoder):
    decoder = seq2seq.BeamSearchDecoder(
        cell=decoder_cell,
        embedding=embedder,
        start_tokens=[target_bos_token_id] * batch_size,
        end_token=target_eos_token_id,
        beam_width=args.beam_width,
        initial_state=decoder_cell.zero_state(
            batch_size=batch_size * args.beam_width, dtype=tf.float32),
        output_layer=training_decoder.output_layer)

    outputs, _, _ = seq2seq.dynamic_decode(
        decoder=decoder, maximum_iterations=60)
    return outputs


def build_model(data_batch, batch_size, source_vocab_size, target_vocab_size,
                target_bos_token_id, target_eos_token_id, advantages, rl_point):
    enc_outputs, enc_last = encode(data_batch, source_vocab_size)

    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(cells=[
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=args.num_units),
            input_keep_prob=tx.utils.switch_dropout(1. - args.dropout)),
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=args.num_units),
            input_keep_prob=tx.utils.switch_dropout(1. - args.dropout))])

    attention_mechanism = seq2seq.LuongAttention(
        num_units=args.num_units,
        memory=tf.cond(
            tf.equal(tx.global_mode(), tf.estimator.ModeKeys.TRAIN),
            lambda: tx.modules.BidirectionalRNNEncoder.concat_outputs(
                enc_outputs),
            lambda: seq2seq.tile_batch(
                tx.modules.BidirectionalRNNEncoder.concat_outputs(enc_outputs),
                args.beam_width)),
        memory_sequence_length=tf.cond(
            tf.equal(tx.global_mode(), tf.estimator.ModeKeys.TRAIN),
            lambda: data_batch['source_length'],
            lambda: seq2seq.tile_batch(
                data_batch['source_length'], args.beam_width)),
        scale=True)

    decoder_cell = seq2seq.AttentionWrapper(
        cell=decoder_cell,
        attention_mechanism=attention_mechanism,
        attention_layer_size=args.num_units)

    embedder = tx.modules.WordEmbedder(
        vocab_size=target_vocab_size, hparams={'dim': args.num_units})

    sampling_output, greedy_output, training_decoder, train_op = get_train_op(
        data_batch, decoder_cell, embedder, enc_outputs, enc_last,
        batch_size, target_vocab_size, advantages, rl_point,
        target_bos_token_id, target_eos_token_id)

    infer_outputs = get_infer_outputs(
        data_batch, decoder_cell, embedder, enc_outputs, enc_last,
        batch_size, target_vocab_size,
        target_bos_token_id, target_eos_token_id, training_decoder)

    return sampling_output, greedy_output, train_op, infer_outputs


def main():
    if args.task == 'mt':
        configs = mt_configs
    else:
        configs = ts_configs

    training_data = \
        tx.data.PairedTextData(hparams=configs.training_data_hparams)
    valid_data = tx.data.PairedTextData(hparams=configs.valid_data_hparams)
    test_data = tx.data.PairedTextData(hparams=configs.test_data_hparams)
    data_iterator = tx.data.TrainTestDataIterator(
        train=training_data, val=valid_data, test=test_data)

    batch_size = training_data.batch_size
    advantages = tf.placeholder(
        dtype=tf.float32, shape=[batch_size], name='advantages')
    rl_point = tf.placeholder(dtype=tf.int32, shape=[], name='rl_point')
    source_vocab_size = training_data.source_vocab.size
    target_vocab_size = training_data.target_vocab.size
    target_bos_token_id = training_data.target_vocab.bos_token_id
    target_eos_token_id = training_data.target_vocab.eos_token_id

    data_batch = data_iterator.get_next()

    sampling_output, greedy_output, train_op, infer_outputs = build_model(
        data_batch, batch_size, source_vocab_size, target_vocab_size,
        target_bos_token_id, target_eos_token_id, advantages, rl_point)

    def train_epoch(sess, epoch, is_xent, rl_p):
        data_iterator.switch_to_train_data(sess)

        log_file = open(log_dir + 'training_log' + str(epoch) + '.txt', 'w')

        counter = 0
        while True:
            try:
                counter += 1
                if is_xent == False:
                    target_texts, sampling_ids, greedy_ids = sess.run(
                        [data_batch['target_text'], sampling_output.sample_id,
                         greedy_output.sample_id],
                        feed_dict={
                            tx.global_mode(): tf.estimator.ModeKeys.TRAIN,
                            rl_point: rl_p})
                    target_texts = target_texts.tolist()
                    sampling_texts = id2texts(
                        sampling_ids, target_eos_token_id,
                        training_data.target_vocab.id_to_token_map_py)
                    greedy_texts = id2texts(
                        greedy_ids, target_eos_token_id,
                        training_data.target_vocab.id_to_token_map_py)

                    advs = []
                    for i in range(len(target_texts)):
                        ref = [target_texts[i][1:target_texts[i].index('<EOS>')]]
                        if args.task == 'mt':
                            advs.append(
                                bleu_score.sentence_bleu(
                                    references=ref,
                                    hypothesis=sampling_texts[i]) -
                                bleu_score.sentence_bleu(
                                    references=ref,
                                    hypothesis=greedy_texts[i]))
                        else:
                            try:
                                rouge = Rouge()
                                advs.append(
                                    rouge.get_scores(
                                        refs=' '.join(ref[0]).decode('utf-8'),
                                        hyps=str(' '.join(sampling_texts[i])),
                                        avg=True)['rouge-2']['f'] -
                                    rouge.get_scores(
                                        refs=' '.join(ref[0]).decode('utf-8'),
                                        hyps=str(' '.join(greedy_texts[i])),
                                        avg=True)['rouge-2']['f'])
                            except:
                                print('except.')
                                assert len(advs) == i
                                advs.append(0.)
                    advs = np.array(advs)
                else:
                    advs = np.zeros([batch_size])

                print(counter, sess.run(train_op, feed_dict={
                    tx.global_mode(): tf.estimator.ModeKeys.TRAIN,
                    rl_point: rl_p, advantages: advs}), file=log_file)
                log_file.flush()
            except tf.errors.OutOfRangeError:
                break

    def id2texts(ids, eos_token_id, id2token_dict):
        result = []
        for i in range(len(ids)):
            result.append([])
            for j in range(len(ids[i])):
                if ids[i][j] == eos_token_id:
                    break
                else:
                    result[-1].append(id2token_dict[ids[i][j]])
        return result

    def valid_epoch(sess):
        data_iterator.switch_to_val_data(sess)

        refs = []
        hypos = []
        while True:
            try:
                target_texts, output_ids = sess.run(
                    [data_batch['target_text'][:, 1:],
                     infer_outputs.predicted_ids[:, :, 0]], feed_dict={
                        tx.global_mode(): tf.estimator.ModeKeys.PREDICT})

                target_texts = target_texts.tolist()
                output_texts = id2texts(
                    output_ids, target_eos_token_id,
                    valid_data.target_vocab.id_to_token_map_py)

                for i in range(len(target_texts)):
                    if args.task == 'mt':
                        refs.append(
                            [target_texts[i][:target_texts[i].index('<EOS>')]])
                        hypos.append(output_texts[i])
                    else:
                        refs.append(' '.join(
                            target_texts[i][:target_texts[i].index(
                                '<EOS>')]).decode('utf-8'))
                        hypos.append(' '.join(output_texts[i]).decode('utf-8'))

                for i in range(batch_size):
                    print(refs[-i])
                    print(hypos[-i])

            except tf.errors.OutOfRangeError:
                break

        if args.task == 'mt':
            return 100. * bleu_score.corpus_bleu(
                list_of_references=refs, hypotheses=hypos)
        else:
            rouge = Rouge()
            return rouge.get_scores(hyps=hypos, refs=refs, avg=True)

    def test_epoch(sess):
        data_iterator.switch_to_test_data(sess)

        refs = []
        hypos = []
        while True:
            try:
                target_texts, output_ids = sess.run(
                    [data_batch['target_text'][:, 1:],
                     infer_outputs.predicted_ids[:, :, 0]], feed_dict={
                        tx.global_mode(): tf.estimator.ModeKeys.PREDICT})

                target_texts = target_texts.tolist()
                output_texts = id2texts(
                    output_ids, target_eos_token_id,
                    valid_data.target_vocab.id_to_token_map_py)

                for i in range(len(target_texts)):
                    if args.task == 'mt':
                        refs.append(
                            [target_texts[i][:target_texts[i].index('<EOS>')]])
                        hypos.append(output_texts[i])
                    else:
                        refs.append(' '.join(
                            target_texts[i][:target_texts[i].index(
                                '<EOS>')]).decode('utf-8'))
                        hypos.append(' '.join(output_texts[i]).decode('utf-8'))
            except tf.errors.OutOfRangeError:
                break

        if args.task == 'mt':
            return 100. * bleu_score.corpus_bleu(
                list_of_references=refs, hypotheses=hypos)
        else:
            rouge = Rouge()
            return rouge.get_scores(hyps=hypos, refs=refs, avg=True)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        sess.run(tf.tables_initializer())

        for i in range(args.xent):
            train_epoch(sess, i, True, 1000)

            if args.task == 'mt':
                valid_scores = []
                test_scores = []
                for j in range(3):
                    valid_scores.append(valid_epoch(sess))
                    test_scores.append(test_epoch(sess))

                print('valid epoch', i, ':', valid_scores,
                      'avg:', sum(valid_scores) / 3.,
                      file=valid_test_log_file)
                print('test epoch', i, ':', test_scores,
                      'avg:', sum(test_scores) / 3.,
                      file=valid_test_log_file)
                print('=' * 100, file=valid_test_log_file)
                valid_test_log_file.flush()
            else:
                valid_scores = valid_epoch(sess)
                test_scores = test_epoch(sess)

                print('valid epoch', i, ':', file=valid_test_log_file)
                for key, value in valid_scores.items():
                    print(key, value, file=valid_test_log_file)
                print('test epoch', i, ':', file=valid_test_log_file)
                for key, value in test_scores.items():
                    print(key, value, file=valid_test_log_file)
                print('=' * 100, file=valid_test_log_file)
                valid_test_log_file.flush()

        epoch_no = args.xent
        for t in range(args.length_t, -1, -args.delta):
            print('====== t =', t, '===============', file=valid_test_log_file)
            for j in range(args.xer):
                train_epoch(sess, epoch_no, False, t)
                epoch_no += 1

                if args.task == 'mt':
                    valid_scores = []
                    test_scores = []
                    for j in range(3):
                        valid_scores.append(valid_epoch(sess))
                        test_scores.append(test_epoch(sess))

                    print('valid epoch', epoch_no, ':', valid_scores,
                          'avg:', sum(valid_scores) / 3.,
                          file=valid_test_log_file)
                    print('test epoch', epoch_no, ':', test_scores,
                          'avg:', sum(test_scores) / 3.,
                          file=valid_test_log_file)
                    print('=' * 100, file=valid_test_log_file)
                    valid_test_log_file.flush()
                else:
                    valid_scores = valid_epoch(sess)
                    test_scores = test_epoch(sess)

                    print('valid epoch', epoch_no, ':', file=valid_test_log_file)
                    for key, value in valid_scores.items():
                        print(key, value, file=valid_test_log_file)
                    print('test epoch', epoch_no, ':', file=valid_test_log_file)
                    for key, value in test_scores.items():
                        print(key, value, file=valid_test_log_file)
                    print('=' * 100, file=valid_test_log_file)
                    valid_test_log_file.flush()


if __name__ == '__main__':
    main()

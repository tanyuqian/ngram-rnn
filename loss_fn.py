import texar as tx
import tensorflow as tf


def ngram_loss_fn(data_batch, output, FLAGS):
    mle_loss = tx.losses.sequence_sparse_softmax_cross_entropy(
        labels=data_batch['target_text_ids'][:, 1:],
        logits=output.logits,
        sequence_length=data_batch['target_length'] - 1)

    loss_f1, loss_f2, loss_f3 = 0., 0., 0.
    if FLAGS.k >= 2:
        loss_f1 = \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=data_batch['target_text_ids'][:, 2:],
                logits=output.logits_f1[:, :-1],
                sequence_length=data_batch['target_length'] - 2) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids0[:, :-1],
                logits=output.logits[:, :-1],
                sequence_length=data_batch['target_length'] - 2)

    if FLAGS.k >= 3:
        loss_f2 = \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=data_batch['target_text_ids'][:, 3:],
                logits=output.logits_f2[:, :-2],
                sequence_length=data_batch['target_length'] - 3) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids1[:, :-2],
                logits=output.logits_f1[:, :-2],
                sequence_length=data_batch['target_length'] - 3) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids0[:, :-2],
                logits=output.logits[:, :-2],
                sequence_length=data_batch['target_length'] - 3)

    if FLAGS.k >= 4:
        loss_f3 = \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=data_batch['target_text_ids'][:, 4:],
                logits=output.logits_f3[:, :-3],
                sequence_length=data_batch['target_length'] - 4) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids2[:, :-3],
                logits=output.logits_f2[:, :-3],
                sequence_length=data_batch['target_length'] - 4) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids1[:, :-3],
                logits=output.logits_f1[:, :-3],
                sequence_length=data_batch['target_length'] - 4) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids0[:, :-3],
                logits=output.logits[:, :-3],
                sequence_length=data_batch['target_length'] - 4)

    return mle_loss + FLAGS.c * loss_f1 + FLAGS.d * loss_f2 + FLAGS.e * loss_f3


def ngram_loss_fn_with_rewards(data_batch, output, training_rewards, FLAGS):
    mle_loss = tx.losses.sequence_sparse_softmax_cross_entropy(
        labels=data_batch['target_text_ids'][:, 1:],
        logits=output.logits,
        sequence_length=data_batch['target_length'] - 1,
        average_across_batch=False)
    
    loss_f1, loss_f2, loss_f3 = 0., 0., 0.
    if FLAGS.k >= 2:
        loss_f1 = \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=data_batch['target_text_ids'][:, 2:],
                logits=output.logits_f1[:, :-1],
                sequence_length=data_batch['target_length'] - 2,
                average_across_batch=False) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids0[:, :-1],
                logits=output.logits[:, :-1],
                sequence_length=data_batch['target_length'] - 2,
                average_across_batch=False)

    if FLAGS.k >= 3:
        loss_f2 = \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=data_batch['target_text_ids'][:, 3:],
                logits=output.logits_f2[:, :-2],
                sequence_length=data_batch['target_length'] - 3,
                average_across_batch=False) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids1[:, :-2],
                logits=output.logits_f1[:, :-2],
                sequence_length=data_batch['target_length'] - 3,
                average_across_batch=False) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids0[:, :-2],
                logits=output.logits[:, :-2],
                sequence_length=data_batch['target_length'] - 3,
                average_across_batch=False)

    if FLAGS.k >= 4:
        loss_f3 = \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=data_batch['target_text_ids'][:, 4:],
                logits=output.logits_f3[:, :-3],
                sequence_length=data_batch['target_length'] - 4,
                average_across_batch=False) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids2[:, :-3],
                logits=output.logits_f2[:, :-3],
                sequence_length=data_batch['target_length'] - 4,
                average_across_batch=False) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids1[:, :-3],
                logits=output.logits_f1[:, :-3],
                sequence_length=data_batch['target_length'] - 4,
                average_across_batch=False) + \
            tx.losses.sequence_sparse_softmax_cross_entropy(
                labels=output.sample_ids0[:, :-3],
                logits=output.logits[:, :-3],
                sequence_length=data_batch['target_length'] - 4,
                average_across_batch=False)

    total_loss = mle_loss + \
                 FLAGS.c * loss_f1 + FLAGS.d * loss_f2 + FLAGS.e * loss_f3

    return tf.reduce_sum(total_loss * training_rewards) / tf.to_float(
        tf.shape(training_rewards)[0]) * FLAGS.n_samples

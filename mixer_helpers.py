from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import abc

import six

from tensorflow.contrib.seq2seq.python.ops import decoder
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.framework import tensor_shape
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import control_flow_ops
from tensorflow.python.ops import embedding_ops
from tensorflow.python.ops import gen_array_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import tensor_array_ops
from tensorflow.python.ops.distributions import bernoulli
from tensorflow.python.ops.distributions import categorical
from tensorflow.python.util import nest

from tensorflow.contrib import seq2seq
import tensorflow as tf


class MixerSamplingHelper(seq2seq.GreedyEmbeddingHelper):
  """A helper for use during inference.

  Uses sampling (from a distribution) instead of argmax and passes the
  result through an embedding layer to get the next input.
  """

  def __init__(self, embedding, inputs, sequence_length,
               rl_point, start_tokens, end_token,
               softmax_temperature=None, seed=None):
    """Initializer.

    Args:
      embedding: A callable that takes a vector tensor of `ids` (argmax ids),
        or the `params` argument for `embedding_lookup`. The returned tensor
        will be passed to the decoder input.
      start_tokens: `int32` vector shaped `[batch_size]`, the start tokens.
      end_token: `int32` scalar, the token that marks end of decoding.
      softmax_temperature: (Optional) `float32` scalar, value to divide the
        logits by before computing the softmax. Larger values (above 1.0) result
        in more random samples, while smaller values push the sampling
        distribution towards the argmax. Must be strictly greater than 0.
        Defaults to 1.0.
      seed: (Optional) The sampling seed.

    Raises:
      ValueError: if `start_tokens` is not a 1D tensor or `end_token` is not a
        scalar.
    """
    super(MixerSamplingHelper, self).__init__(
        embedding, start_tokens, end_token)
    self._inputs = inputs
    self._rl_point = rl_point
    self._sequence_length = ops.convert_to_tensor(
        sequence_length, name="sequence_length")

  def sample(self, time, outputs, state, name=None):
    """sample for SampleEmbeddingHelper."""
    # del time, state  # unused by sample_fn
    # Outputs are logits, we sample instead of argmax (greedy).
    if not isinstance(outputs, ops.Tensor):
      raise TypeError("Expected outputs to be a single Tensor, got: %s" %
                      type(outputs))
    logits = outputs

    sample_id_sampler = categorical.Categorical(logits=logits)
    sample_ids = sample_id_sampler.sample()

    # print(self._inputs[:, time], sample_ids)
    # print(time, self._rl_point)
    # exit(0)

    return tf.cond(
        tf.less(time, self._rl_point),
        lambda : tf.to_int32(self._inputs[:, time + 1]),
        lambda : sample_ids)
    # return sample_ids

  def next_inputs(self, time, outputs, state, sample_ids, name=None):
    """next_inputs_fn for GreedyEmbeddingHelper."""
    del outputs  # unused by next_inputs_fn
    finished = math_ops.equal(sample_ids, self._end_token)

    next_time = time + 1
    finished = tf.logical_or((next_time >= self._sequence_length), finished)

    all_finished = math_ops.reduce_all(finished)
    next_inputs = control_flow_ops.cond(
        all_finished,
        # If we're finished, the next_inputs value doesn't matter
        lambda: self._start_inputs,
        lambda: self._embedding_fn(sample_ids))
    return (finished, next_inputs, state)


class MixerGreedyHelper(seq2seq.GreedyEmbeddingHelper):
  """A helper for use during inference.

  Uses sampling (from a distribution) instead of argmax and passes the
  result through an embedding layer to get the next input.
  """

  def __init__(self, embedding, inputs, sequence_length,
               rl_point, start_tokens, end_token,
               softmax_temperature=None, seed=None):
    """Initializer.

    Args:
      embedding: A callable that takes a vector tensor of `ids` (argmax ids),
        or the `params` argument for `embedding_lookup`. The returned tensor
        will be passed to the decoder input.
      start_tokens: `int32` vector shaped `[batch_size]`, the start tokens.
      end_token: `int32` scalar, the token that marks end of decoding.
      softmax_temperature: (Optional) `float32` scalar, value to divide the
        logits by before computing the softmax. Larger values (above 1.0) result
        in more random samples, while smaller values push the sampling
        distribution towards the argmax. Must be strictly greater than 0.
        Defaults to 1.0.
      seed: (Optional) The sampling seed.

    Raises:
      ValueError: if `start_tokens` is not a 1D tensor or `end_token` is not a
        scalar.
    """
    super(MixerGreedyHelper, self).__init__(embedding, start_tokens, end_token)
    self._inputs = inputs
    self._rl_point = rl_point
    self._sequence_length = ops.convert_to_tensor(
        sequence_length, name="sequence_length")

  def sample(self, time, outputs, state, name=None):
    """sample for GreedyEmbeddingHelper."""
    del state  # unused by sample_fn
    # Outputs are logits, use argmax to get the most probable id
    if not isinstance(outputs, ops.Tensor):
      raise TypeError("Expected outputs to be a single Tensor, got: %s" %
                      type(outputs))
    sample_ids = math_ops.argmax(outputs, axis=-1, output_type=dtypes.int32)

    return tf.cond(
        tf.less(time, self._rl_point),
        lambda : tf.to_int32(self._inputs[:, time + 1]),
        lambda : sample_ids)

  def next_inputs(self, time, outputs, state, sample_ids, name=None):
    """next_inputs_fn for GreedyEmbeddingHelper."""
    del outputs  # unused by next_inputs_fn
    finished = math_ops.equal(sample_ids, self._end_token)

    next_time = time + 1
    finished = tf.logical_or((next_time >= self._sequence_length), finished)

    all_finished = math_ops.reduce_all(finished)
    next_inputs = control_flow_ops.cond(
        all_finished,
        # If we're finished, the next_inputs value doesn't matter
        lambda: self._start_inputs,
        lambda: self._embedding_fn(sample_ids))
    return (finished, next_inputs, state)
